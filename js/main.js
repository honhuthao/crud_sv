var dssv = [];

//lấy dữu liệu từ local Storage  lúc user load trang
var dataJson = localStorage.getItem("DSSV");
if(dataJson != null)
{
    dssv = JSON.parse(dataJson).map(function(item){
        return new SinhVien(item.ma,item.ten,item.email,item.matKhau,item.toan,item.ly,item.hoa);
    });
    //map
    renderDSSV(dssv);
}

function themSinhVien(){
    //lấy thông tin từ form
    var sv = layThongTinTuForm();
    if(!validateMaSV(sv.ma)){
        return;
    }
    dssv.push(sv);
    
    //render dssv: đưa data lên giao diện
    renderDSSV(dssv);

    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJson);
}

function xoaSinhVien(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id;
    });
    dssv.splice(index,1);
    renderDSSV(dssv);
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJson);
}

function suaSinhVien(id){
    var index = dssv.findIndex(function (item){
        return item.ma == id;
    });
    //show thông tin lên form
    showThongTinLenForm(dssv[index]);
}

function capNhat(){
    var ttsv = layThongTinTuForm();
    var index = dssv.findIndex(function (item){
        return item.ma == ttsv.ma;
    });

    var sv_new = layThongTinTuForm();
    dssv[index] = sv_new;
    renderDSSV(dssv)
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV",dataJson);
}

function timSV(){
    var tenSV = document.getElementById("txtSearch").value;
    var dssvTimKiem = dssv.filter(function(item){
        return item.ten.toLowerCase().indexOf(tenSV.toLowerCase()) > -1;
    }
    );
    renderDSSV(dssvTimKiem);
}

function reset(){
    document.getElementById("formSinhVien").reset();
}
