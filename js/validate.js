// validate mã sv
function validateMaSV(maSV){
    // kiểm tra rỗng
    if (maSV == ""){
        showMessage("spanMaSV","Mã sinh viên không được rỗng");
        return false;
    }
    // kiểm tra trùng
    var isExist = dssv.some(function(item){
        return item.ma == maSV;
    }
    );
    if (isExist){
        showMessage("spanMaSV","Mã sinh viên đã tồn tại");
        return false;
    }
    else{
        showMessage("spanMaSV","");
    }
    // kiểm tra định dạng
    var re = /^[0-9]+$/;
    if (re.test(maSV)){
        showMessage("spanMaSV","");
        return true;
    }
    else{
        showMessage("spanMaSV","Mã sinh viên phải là số");
        return false;
    }

}